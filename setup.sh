#!/usr/bin/env bash

source_directory=$(realpath $(dirname "${BASH_SOURCE}"))

source "${source_directory}/bashrc_utils.sh"
source "${source_directory}/directory_utils.sh"

if [ -z "$ASHM_UTILS_BASHRC" ]; then

    source "./bashrc_utils.sh"
fi

if [ -z "$ASHM_UTILS_DIRECTORIES" ]; then

    source "./directory_utils.sh"
fi

if [ -z "$XDG_BIN_HOME" ] || ! [ -d "$XDG_BIN_HOME" ]; then

    echo "Error: XDG_BIN_HOME has not been setup"
    exit 1;
fi


function setup_utils() {

    script_filename=$(basename "$1")
    script_directory=$(realpath $(dirname "${BASH_SOURCE}"))

    link_target="${script_directory}/${script_filename}"
    link_name="${XDG_BIN_HOME}/${script_filename}"


    # install script
    rm -f "$link_name"
    ln -s "$link_target" "$link_name" 
    source_file_in_bashrc_section "$script_filename" "bash-utils"
}

function install_man() {

    man_file_name=$(basename "$1")
    man_file_directory=$(realpath $(dirname "${BASH_SOURCE}"))

    gzip -c "${man_file_directory}/${man_file_name}" > "${man_file_directory}/${man_file_name}.gz"
    mv -f "${man_file_directory}/${man_file_name}.gz" "${ASHM_LOCAL_MANPAGE_HOME}/$2"
}

add_section_to_bashrc "bash-utils"

setup_utils "./bashrc_utils.sh"
install_man "./bashrc_utils.7" "man7"

setup_utils "./directory_utils.sh"
install_man "./directory_utils.7" "man7"

setup_utils "./remotes_utils.sh"
install_man "./remotes_utils.7" "man7"

