#!/usr/bin/env bats

function setup() {

    rm -f ./tests/.bashrc
    cp ./tests/template.bashrc ./tests/.bashrc
    source ./bashrc_utils.sh
    export HOME=./tests
}

@test "if the variable already exists it will be updated in place" {

    # arrange
    value=$(date)

    # act
    add_environment_variable_in_bashrc_section "b" "$value" "b"

    # assert
    sed -n '/# b/,/^$/p' ./tests/.bashrc | grep -q "export b=\"$value\""
    [ "$?" -eq 0 ]
}

@test "if add to the start it should be the first line in the section" {

    # arragne
    value=$(date)

    # act
    add_environment_variable_in_bashrc_section "a" "$value" "a" "start"

    # assert
    sed -n '/# a/!b;n;p;q' ./tests/.bashrc | grep -q "export a=\"$value\""
    [ "$?" -eq 0 ]
}

@test "if add to the end it should be the last line in the section" {

    # arrange
    value=$(date)

    # act
    add_environment_variable_in_bashrc_section "c" "$value" "b" "end"

    cat ${HOME}/.bashrc

    # assert
    sed -n '/# b/!b;:a;n;/./h;/./ba;x;p;q' ./tests/.bashrc | grep -q "export c=\"$value\""
    [ "$?" -eq 0 ]
}

@test "if the position is not specified it is added to the end" {

    # arrange
    value=$(date)

    # act
    add_environment_variable_in_bashrc_section "c" "$value" "b"

    # assert
    sed -n '/# b/!b;:a;n;/./h;/./ba;x;p;q' ./tests/.bashrc | grep -q "export c=\"$value\""
    [ "$?" -eq 0 ]
}


@test "if the position is neither start nor end it is added to the end" {

    # arragne
    value=$(date)

    # act
    add_environment_variable_in_bashrc_section "c" "$value" "b" "not a position"

    # assert
    sed -n '/# b/!b;:a;n;/./h;/./ba;x;p;q' ./tests/.bashrc | grep -q "export c=\"$value\""
    [ "$?" -eq 0 ]
}

@test "if the varaible name is not specified the function reports an error" {

    # arrange

    # act
    run add_environment_variable_in_bashrc_section

    # assert
    [ "$status" -eq 1 ]
}

@test "if the value is not specified the function reports an error" {

    # arrange

    # act
    run add_environment_variable_in_bashrc_section "name"

    # assert
    [ "$status" -eq 1 ]
}

@test "if the section name is not specified the function reports an error" {

    # arrange

    # act
    run add_environment_variable_in_bashrc_section "name" "value"

    # assert
    [ "$status" -eq 1 ]
}
