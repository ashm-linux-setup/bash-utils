#!/usr/bin/env bats

function setup() {

    rm -f ./tests/.bashrc
    cp ./tests/template.bashrc ./tests/.bashrc
    source ./bashrc_utils.sh
    export HOME=./tests
}

# if the file is already sourced in the section it will not be added

@test "the file will be sources in the first blank line after the section header" {

    # arrange

    # act
    source_file_in_bashrc_section "test.sh" "c"

    # assert
    last_line_in_section=$(\
        sed -n '/# c/,/^$/p' "${HOME}/.bashrc"  |\
	grep . |\
	tail -n 1
    )

    echo $last_line_in_section | grep "source test.sh"
}


@test "if the files is already sources in the section it will not be added twice" {

    # arrange
    source="source test.sh"
    echo "$source" >> "${HOME}/.bashrc"
    
    # act
    source_file_in_bashrc_section "test.sh" "c"
    
    # assert
    number_of_lines_sourcing_file=$(\
       sed -n '/# c/,/^$/p' "${HOME}/.bashrc" | \
       grep "$source" | \
       wc -l
    )

    [ $number_of_lines_sourcing_file -eq 1 ] 
}

@test "if the file name is not specified an error is reported" {

    # arrange
    
    # act
    run source_file_in_bashrc_section

    # assert
    [ "$status" -eq 1 ]
}

@test "if the section is not specifie an error is reported" {

    # arrange

    # act
    run source_file_in_bashrc_section "file.sh"

    # assert
    [ "$status" -eq 1 ]
}
