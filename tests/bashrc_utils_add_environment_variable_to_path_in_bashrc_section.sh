#!/usr/bin/env bats

function setup() {

    rm -f ./tests/.bashrc
    cp ./tests/template.bashrc ./tests/.bashrc
    source ./bashrc_utils.sh
    export HOME=./tests
}


@test "the variable is added to the path in the section" {

    # arrange

    # act
    include_environment_variable_in_path_in_bashrc_section "b" "c"

    # assert
    occurrences=$(\
        sed -n '/# c/,/^$/p' "${HOME}/.bashrc" | \
        grep 'export PATH=$b:$PATH' | \
	wc -l
    )

    echo $occurrences

    [ "$occurrences" -eq 1 ]

}

@test "if the variable has already been added to the path in that section it is not readded" {

    # arrange
    export="export PATH=\$b:\$PATH"
    sed -i "/# c/a $export" "${HOME}/.bashrc"


    # act
    include_environment_variable_in_path_in_bashrc_section "b" "c"


    # assert
    occurrences=$(\
        sed -n "/# c/,/^$/p" "${HOME}/.bashrc" | \
	grep "$export" | \
	wc -l
    )

    echo $occurrences

    [ $occurrences -eq 1 ]
}


# if the variable has already been added to the path in that section it is not re added.
