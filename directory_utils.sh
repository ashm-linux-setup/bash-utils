#!/usr/bin/env bash
#
# Common directory utilities that are used in setup/config scripts

export ASHM_UTILS_DIRECTORIES='y'


function help_for_ashm_utils_directories() {

    echo "Feature Flag    ,ASHM_UTILS_DIRECTORIES"
    echo "Function        ,create_dir_if_not_exist"
    echo "Function        ,source_aliases"
}
export -f help_for_ashm_utils_directories


function create_directory_if_not_exist() {

    if [ -z "$1" ]; then

        echo "Error: no directory path supplied"
	return 1
    fi


    if [ ! -d "$1" ]; then

        mkdir "$1"
        echo "Created dir: $1"
    fi
}
export -f create_directory_if_not_exist


function source_aliases() {

    if [ -f $1/.bash_aliases ]; then

        source $1/.bash_aliases
    fi

    if [ -f $1/alias.sh ]; then

        source $1/alias.sh
    fi
}
export -f source_aliases
