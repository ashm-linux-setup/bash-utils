#!/usr/bin/env bash
#
# Common bashrc utilities that are used in setup/config scripts

export ASHM_UTILS_BASHRC='y'

function error() {

    echo "Error [${FUNCNAME[1]}]: $1" >&2
}


function help_for_ashm_utils_bashrc() {

    echo "Feature Flag   ,ASHM_UTILS_BASHRC"
    echo "Function       ,add_section_to_bashrc"
    echo "Function       ,add_environment_var_to_bashrc"
    echo "Function       ,add_var_to_path"
}
export -f help_for_ashm_utils_bashrc


function ensure_file_is_terminated_with_a_blank_line() {

    # ensure .bashrc file is correctly terminated with a blank line
    if tail -n 1 "${1}" | grep -q "."; then

        echo "" >> "${1}"
    fi
}
export -f ensure_file_is_terminated_with_a_blank_line


function add_section_to_bashrc() {

    if grep -q "# $1" "$HOME/.bashrc"; then

        return 0
    fi

    echo "" >> "$HOME/.bashrc"
    echo "# $1" >> "$HOME/.bashrc"
}
export -f add_section_to_bashrc


function add_environment_variable_in_bashrc_section() {

    # Adds an environment variable to a section (/# <section name>\n)
    # if the variable already exist it will update it otherwise
    # it allows you to specify if you want it to be added at
    # the start or the end of the section.
    #
    # n. it does not use named options because I was lazy


    # variable name
    if [ -z "$1" ]; then

	error "Variable name not speficfied in \$1"
        return 1
    fi
    # variable value
    if [ -z "$2" ]; then

        error "Value not specified in \$2"
	return 1
    fi
    # section name
    if [ -z "$3" ]; then

	error "Section not specified in \$3"
	return 1
    fi

    insert_position="start"
    if [ "$4" != "start" ]; then

        insert_position="end"
    fi


    ensure_file_is_terminated_with_a_blank_line "${HOME}/.bashrc"

    # if variable already exists in the section update it in place
    if sed -n "/# ${3}/,/^$/p" "${HOME}/.bashrc" | grep -q "export ${1}="; then

        sed -i -E "/# $3/,/^$/ s|(export ${1}=).*|\1\"${2}\"|" "${HOME}/.bashrc"

    # if add to start it add it on the first line after the section header
    elif [ "$insert_position" == "start" ]; then

	sed -i -E "/# $3/a export ${1}=\"${2}\"" "${HOME}/.bashrc"

    # if add to end it add it to the first blank line after the section header
    elif [ "$insert_position" == "end" ]; then

	sed -i -E "/# $3/!b;:a;n;/./ba;i export ${1}=\"${2}\"" "${HOME}/.bashrc"
    fi
}
export -f add_environment_variable_in_bashrc_section


function include_environment_variable_in_path_in_bashrc_section() {

    # includes an environment variable in the path within a section
    # (/# <section name>\n).  If the variable has already been included
    # in the path within that section no changes are applied otherwise
    # it is added at the end of the section (first blank line after
    # the section header).

    export="export PATH=\$${1}:\$PATH"

    # if not already in the section add the include at the end
    if ! sed -n "/# $2/,/^$/p" "${HOME}/.bashrc" | grep -q "$export"; then

	# ensure .bashrc file is correctly terminated with blank last line
	ensure_file_is_terminated_with_a_blank_line "${HOME}/.bashrc"

	sed -i "/# $2/!b;:a;n;/./ba;i $export" "${HOME}/.bashrc"
    fi
}
export -f include_environment_variable_in_path_in_bashrc_section


function source_file_in_bashrc_section() {

    # sources a file within a section (/# <section name>\n). If
    # the file is already sources in that section it will not
    # be sources twice otherwise it sources the file at the end
    # of the section (first blank line after the section header).

    if [ -z "$1" ]; then

        error "File path needs to be supplied in \$1"
	return 1
    fi

    if [ -z "$2" ]; then

        error "Section needs to be supplied in \$2"
	return 1
    fi


    source="source $1"

    # if the file is not already sourced in the section source it
    if ! sed -n "/# $2/,/^$/p" "${HOME}/.bashrc" | grep -q "$source"; then

        ensure_file_is_terminated_with_a_blank_line "${HOME}/.bashrc"
        sed -i  "/# $2/!b;:a;n;/./ba;i $source >> /dev/null" "${HOME}/.bashrc"
    fi
}
export -f source_file_in_bashrc_section

