#!/usr/bin/env bash
#
# Common remotes utilities that are used in setup/config scripts.
#
# n. remotes in this case are the repositories that are downloaded
#    and installed when configuring a linux environment.

export ASHM_UTILS_REMOTES='y'

# get_remote_name
# get_or_update_remote
# install_remote

function error() {

    echo "remote_utils [${FUNCNAME[1]}]: $1" >&2
}

function get_remote_name() {

    # guard
    if [ -z "$1" ]; then

        error "remote repo url not specified in \$1"
	return 1
    fi

    # act
    basename "$1" .git
}
export -f get_remote_name


function get_remote_directory_path() {

    # guard
    if [ -z "$1" ]; then

        error "remote repo url not specified in \$1"
	return 1
    fi

    # act
    remote_directory_name=$(get_remote_name "$1")
    echo "${ASHM_REMOTES_HOME}/${remote_directory_name}"
}
export -f get_remote_directory_path


function get_or_update_remote() (

    # guard
    if [ -z "$ASHM_REMOTES_HOME" ] || ! [ -d "$ASHM_REMOTES_HOME" ]; then

        error "ASHM_REMOTES_HOME not setup"
	return 1
    fi

    if [ -z "$1" ]; then

        error "remote repo url not specified in \$1"
	return 1
    fi


    # act
    cd "$ASHM_REMOTES_HOME"

    remote_directory="$(get_remote_name "$1")"

    if [ ! -d "${ASHM_REMOTES_HOME}/${remote_directory}" ]; then

        git clone "$1" "$remote_directory"
    else

        cd "${ASHM_REMOTES_HOME}/${remote_directory}"
        git pull
    fi
)
export -f get_or_update_remote


function install_remote() {

    # guard
    if [ -z "$ASHM_REMOTES_HOME" ] || ! [ -d "$ASHM_REMOTES_HOME" ]; then

        error "ASHM_REMOTES_HOME not setup"
	return 1
    fi

    if [ -z "$1" ]; then

        error "remote repo url not specified in \$1"
	return 1
    fi


    # act
    remote_directory=$(get_remote_name "$1")

    if [ -e "${ASHM_REMOTES_HOME}/${remote_directory}/setup.sh" ]; then

       "${ASHM_REMOTES_HOME}/${remote_directory}/setup.sh"
    fi
}
export -f install_remote

