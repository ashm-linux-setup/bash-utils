# Bash scripting utilities

Set of scripting utilitiy files that can be sources for access.


## Utilities

List entry contains the script name and the feature flag that identifies if it has been loaded.

- bashrc_utils.sh     ,ASHM_UTILS_BASHRC
- directory_utils.sh  ,ASHM_UTILS_DIRECTORIES
- remotes_utils.sh    ,ASHM_UTILS_REMOTES

